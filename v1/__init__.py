import os
import inspect
import random
import string
from functools import wraps

from flask import request
from flask_restful import Api, Resource
import docker as docker

from .db import init_db, db
from . import response


permissions = []
client = docker.DockerClient(base_url='unix://var/run/docker.sock')


def api_init(app, path):
	db = init_db(app)

	api = Api(app)

	# Find all classes in a module that are subclasses of "c"
	def find_classes(c, module):
		for name, obj in inspect.getmembers(module):
			if inspect.isclass(obj) and obj != c and issubclass(obj, c):
				yield obj

	# Register API endpoints.
	def register_apis(api, module):
		def invalid_method(*args, **kwargs):
			return response.invalid_method()

		for obj in find_classes(Resource, module):
			for method in ['get', 'post', 'put', 'delete', 'patch', 'options']:
				if not hasattr(obj, method):
					setattr(obj, method, invalid_method)
				else:
					setattr(obj, method, wrap_docker(getattr(obj, method)))

			http_dir = api_id.replace('.', '/')

			api.add_resource(obj, '/%s%s' % (http_dir, obj._endpoint))
			yield '/%s%s <%s>' % (http_dir, obj._endpoint, obj.__name__)

	for root, dirs, files in os.walk(path):
		if '__pycache__' in root: continue

		for dir in dirs:
			if dir == '__pycache__': continue

			api_id = root.replace('/', '.') + '.' + dir

			print(api_id)

			module = __import__(api_id, fromlist=[''])

			for c in register_apis(api, module):
				print('> Registering endpoint ' + c)

	print('Permissions: ' + ', '.join(permissions))

	db.create_all()

	class BaseEndpoint(Resource):
		_endpoint = '/'

		def get(self):
			return response.success({'response': 0})
	api.add_resource(BaseEndpoint, '/')

	return api

def gen_id(length=8):
	return ''.join(random.choices(string.ascii_uppercase + string.digits, k=length))

def wrap_docker(func):
	def inner(*args, **kwargs):
		try:
			return func(*args, **kwargs)
		except docker.errors.APIError as e:
			return {'status': e.status_code, 'message': e.explanation}, e.status_code
	return inner


# oPX15QuiIpu7cF1XM0YmQHhreWSG5hvJ
def require_key(func):
	from .users.table import User, AccessKey
	def inner(*args, **kwargs):
		key = request.args.get('access_key')
		if key is None: return response.unauthorized()

		access = AccessKey.query.filter_by(key_hash=AccessKey.gen_hash(key)).first()
		if access is None: return response.not_found('Invalid access key.')

		if not access.validate(): return response.unauthorized()

		return func(args[0], User.query.filter_by(user_id=access.user_id).first(), *args[1:], **kwargs)
	return inner

def permission(node):
	permissions.append(node)

	def inner(func):
		@wraps(func)
		def wrap(*args, **kwargs):
			user = args[1]
			for perm in user.role.permissions:
				if perm.node == '*':
					break
				if perm.node == node:
					break
			else:
				return response.forbidden()
			return func(*args, **kwargs)
		return wrap
	return inner
