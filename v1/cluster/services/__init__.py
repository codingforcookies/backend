from flask import request
from flask_restful import Resource
from sqlalchemy.exc import IntegrityError

import docker

from ... import client, gen_id, require_key, permission, response
from ...db import db


def as_dict(s):
	return {
		'short_id': s.short_id,
		'created': s.attrs['CreatedAt'],
		'updated': s.attrs['UpdatedAt'],

		'image': s.attrs['Spec']['TaskTemplate']['ContainerSpec']['Image'],
		'replicas': s.attrs['Spec']['Mode']['Replicated']['Replicas'],

		'name': s.attrs['Labels']['name'] if 'Labels' in s.attrs and s.attrs['Labels'] is not None and 'name' in s.attrs['Labels'] else 'Unnamed Image',
		'info': s.attrs['Labels']['info'] if 'Labels' in s.attrs and s.attrs['Labels'] is not None and 'info' in s.attrs['Labels'] else ''
	}

class ServiceList(Resource):
	_endpoint = ''

	@require_key
	@permission('services.list')
	def get(self, user):
		return response.success({
					'result': {s.id: as_dict(s) for s in client.services.list()}
				})

	@require_key
	@permission('services.new')
	def post(self, user):
		data = request.get_json(silent=True)
		if data is None: return response.bad_request('JSON data must be supplied.')

		if 'image' not in data: return response.bad_request('You must supply an image to create the service.')
		if 'replicas' not in data: return response.bad_request('You must specify the number of replicas to spawn.')

		s = client.services.create(data['image'], mode=docker.types.ServiceMode('replicated', replicas=data['replicas']))
		return response.created({'result': {s.id: as_dict(s)}}, '/services/%s' % s.id)

class ServiceSelect(Resource):
	_endpoint = '/<service_id>'

	@require_key
	@permission('services.select')
	def get(self, user, service_id):
		try:
			s = client.services.get(service_id)

			return response.success({'result': {s.id: as_dict(s)}})
		except docker.errors.NotFound as e:
			return response.not_found('A service with that ID does not exist.')

	@require_key
	@permission('services.delete')
	def delete(self, user, service_id):
		try:
			client.services.get(service_id).remove()

			return response.no_content()
		except docker.errors.NotFound as e:
			return response.not_found('A service with that ID does not exist.')

class ServiceLogs(Resource):
	_endpoint = '/<service_id>/scale'

	@require_key
	@permission('services.select')
	def get(self, user, service_id):
		try:
			return response.success({'result': client.services.get(service_id).logs().split('\n')})
		except docker.errors.NotFound as e:
			return response.not_found('A service with that ID does not exist.')

class ServiceLogs(Resource):
	_endpoint = '/<service_id>/logs'

	@require_key
	@permission('services.select')
	def get(self, user, service_id):
		try:
			return response.success({'result': client.services.get(service_id).logs().split('\n')})
		except docker.errors.NotFound as e:
			return response.not_found('A service with that ID does not exist.')
