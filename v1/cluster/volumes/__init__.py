from flask import request
from flask_restful import Resource
from sqlalchemy.exc import IntegrityError

import docker

from ... import client, gen_id, require_key, permission, response
from ...db import db


def as_dict(v):
	return {
		'short_id': v.short_id,
		'created': c.attrs['Created'],

		'name': v.attrs['Labels']['name'] if 'Labels' in v.attrs and v.attrs['Labels'] is not None and 'name' in v.attrs['Labels'] else 'Unnamed Volume',
		'info': v.attrs['Labels']['info'] if 'Labels' in v.attrs and v.attrs['Labels'] is not None and 'info' in v.attrs['Labels'] else ''
	}

class VolumeList(Resource):
	_endpoint = ''

	@require_key
	@permission('volumes.list')
	def get(self, user):
		return response.success({
					'result': {v.id: as_dict(v) for v in client.volumes.list()}
				})

	@require_key
	@permission('volumes.new')
	def post(self, user):
		data = request.get_json(silent=True)
		if data is None: return response.bad_request('JSON data must be supplied.')

		labels = {}

		if 'name' in data:
			if '/' in data['name']: return response.bad_request('Name cannot contain "/".')
			labels['name'] = data['name']

		if 'info' in data:
			labels['info'] = data['info']

		v = client.volumes.create(gen_id(32), labels=labels)
		return response.created({'result': {v.id: as_dict(v)}}, '/volumes/%s' % v.id)

	@require_key
	@permission('volumes.prune')
	def delete(self, user):
		client.volumes.prune()
		return response.no_content()

class VolumeSelect(Resource):
	_endpoint = '/<volume_id>'

	@require_key
	@permission('volumes.select')
	def get(self, user, volume_id):
		try:
			v = client.volumes.get(volume_id)

			return response.success({'result': {v.id: as_dict(v)}})
		except docker.errors.NotFound as e:
			return response.not_found('A volume with that ID does not exist.')

	@require_key
	@permission('volumes.update')
	def put(self, user, volume_id):
		data = request.get_json(silent=True)
		if data is None: return response.bad_request('JSON data must be supplied.')

		try:
			v = client.volumes.get(volume_id)

			if v.attrs['Labels'] is None:
				v.attrs['Labels'] = {}

			if 'name' in data:
				v.attrs['Labels']['name'] = data['name']

			if 'info' in data:
				v.attrs['Labels']['info'] = data['info']

			v.reload()

			return response.success({'result': {v.id: as_dict(v)}})
		except docker.errors.NotFound as e:
			v = client.volumes.create(gen_id(32), labels={'name': data['name']})
			return response.created({'result': {v.id: as_dict(v)}}, '/volumes/%s' % v.id)

	@require_key
	@permission('volumes.delete')
	def delete(self, user, volume_id):
		try:
			client.volumes.get(volume_id).remove()

			return response.no_content()
		except docker.errors.NotFound as e:
			return response.not_found('A volume with that ID does not exist.')
