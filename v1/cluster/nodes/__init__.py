from flask import request
from flask_restful import Resource
from sqlalchemy.exc import IntegrityError

import docker

from ... import client, gen_id, require_key, permission, response
from ...db import db


def as_dict(n):
	return {
		'short_id': n.short_id,
		'role': n.attrs['Spec']['Role'],
		'status': n.attrs['Status']['State'],
		'ip': n.attrs['Status']['Addr'],
		'hostname': n.attrs['Description']['Hostname'],
		'platform': {'arch': n.attrs['Description']['Platform']['Architecture'], 'os': n.attrs['Description']['Platform']['OS']},
		'resources': {'cpu': n.attrs['Description']['Resources']['NanoCPUs'], 'mem': n.attrs['Description']['Resources']['MemoryBytes']}
	}

class NodeList(Resource):
	_endpoint = ''

	@require_key
	@permission('nodes.list')
	def get(self, user):
		return response.success({
					'result': {n.id: as_dict(n) for n in client.nodes.list()}
				})

class NodeSelect(Resource):
	_endpoint = '/<node_id>'

	@require_key
	@permission('nodes.select')
	def get(self, user, node_id):
		try:
			n = client.nodes.get(node_id)

			return response.success({'result': {n.id: as_dict(n)}})
		except docker.errors.NotFound as e:
			return response.not_found('A node with that ID does not exist.')
