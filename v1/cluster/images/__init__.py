from flask import request
from flask_restful import Resource
from sqlalchemy.exc import IntegrityError

import docker

from ... import client, gen_id, require_key, permission, response
from ...db import db


def as_dict(i):
	return {
		'short_id': i.short_id,
		'tags': i.attrs['RepoTags'],
		'platform': {'arch': i.attrs['Architecture'], 'os': i.attrs['Os']},
		'size': i.attrs['Size'],

		'name': i.attrs['Labels']['name'] if 'Labels' in i.attrs and i.attrs['Labels'] is not None and 'name' in i.attrs['Labels'] else 'Unnamed Image',
		'info': i.attrs['Labels']['info'] if 'Labels' in i.attrs and i.attrs['Labels'] is not None and 'info' in i.attrs['Labels'] else ''
	}

class ImageList(Resource):
	_endpoint = ''

	@require_key
	@permission('images.list')
	def get(self, user):
		return response.success({
					'result': {i.id: as_dict(i) for i in client.images.list()}
				})

	@require_key
	@permission('images.prune')
	def delete(self, user):
		client.images.prune()
		return response.no_content()

class ImageSelect(Resource):
	_endpoint = '/<image_id>'

	@require_key
	@permission('images.select')
	def get(self, user, image_id):
		try:
			i = client.images.get(image_id)

			return response.success({'result': {i.id: as_dict(i)}})
		except docker.errors.NotFound as e:
			return response.not_found('A image with that ID does not exist.')

	@require_key
	@permission('images.new')
	def post(self, user, image_id):
		i = client.images.pull(image_id)
		return response.created({'result': {i.id: as_dict(i)}}, '/images/%s' % i.id)

	@require_key
	@permission('images.update')
	def put(self, user, image_id):
		data = request.get_json(silent=True)
		if data is None: return response.bad_request('JSON data must be supplied.')

		try:
			i = client.images.get(image_id)

			if i.attrs['Labels'] is None:
				i.attrs['Labels'] = {}

			if 'name' in data:
				i.attrs['Labels']['name'] = data['name']

			if 'info' in data:
				i.attrs['Labels']['info'] = data['info']

			i.reload()

			return response.success({'result': {i.id: as_dict(i)}})
		except docker.errors.NotFound as e:
			i = client.images.create(gen_id(32), labels={'name': data['name']})
			return response.created({'result': {i.id: as_dict(i)}}, '/images/%s' % i.id)

	@require_key
	@permission('images.delete')
	def delete(self, user, image_id):
		try:
			client.images.get(image_id).remove()

			return response.no_content()
		except docker.errors.NotFound as e:
			return response.not_found('A image with that ID does not exist.')
