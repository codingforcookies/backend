from flask import request
from flask_restful import Resource
from sqlalchemy.exc import IntegrityError

import docker

from ... import client, gen_id, require_key, permission, response
from ...db import db


def as_dict(c):
	return {
		'short_id': c.short_id,
		'created': c.attrs['Created'],
		'started': c.attrs['State']['StartedAt'],
		'finished': c.attrs['State']['FinishedAt'],
		'status': c.attrs['State']['Status'],
		'image': c.attrs['Image'],
		'platform': {'os': c.attrs['Platform']},

		'state': {
			'running': c.attrs['State']['Running'],
			'paused': c.attrs['State']['Paused'],
			'restarting': c.attrs['State']['Restarting'],
			'killed': c.attrs['State']['OOMKilled'],
			'dead': c.attrs['State']['Dead']
		},

		'stats': {
			'cpu': c.attrs['HostConfig']['CpuPercent'],
			'mem': c.attrs['HostConfig']['Memory']
		}
	}

class ContainerList(Resource):
	_endpoint = ''

	@require_key
	@permission('containers.list')
	def get(self, user):
		return response.success({
					'result': {c.id: as_dict(c) for c in client.containers.list(all=True)}
				})

	@require_key
	@permission('containers.new')
	def post(self, user):
		data = request.get_json(silent=True)
		if data is None: return response.bad_request('JSON data must be supplied.')

		if 'image' not in data: return response.bad_request('You must supply an image to create the container.')

		run = data['run'] if 'run' in data else True

		c = client.containers.run(data['image'], detatch=True) if run else client.containers.create(data['image'])
		return response.created({'result': {c.id: as_dict(c)}}, '/containers/%s' % c.id)

class ContainerSelect(Resource):
	_endpoint = '/<container_id>'

	@require_key
	@permission('containers.select')
	def get(self, user, container_id):
		try:
			c = client.containers.get(container_id)

			return response.success({'result': {c.id: as_dict(c)}})
		except docker.errors.NotFound as e:
			return response.not_found('A container with that ID does not exist.')

	@require_key
	@permission('containers.delete')
	def delete(self, user, container_id):
		try:
			client.containers.get(container_id).remove()

			return response.no_content()
		except docker.errors.NotFound as e:
			return response.not_found('A container with that ID does not exist.')

class ContainerLogs(Resource):
	_endpoint = '/<container_id>/<action>'

	@require_key
	@permission('containers.action')
	def post(self, user, container_id, action):
		try:
			c = client.containers.get(container_id)

			if action == 'start':
				c.start()
			elif action == 'restart':
				c.restart()
			elif action == 'stop':
				c.stop()
			elif action == 'kill':
				c.kill()
			elif action == 'pause':
				c.pause()
			elif action == 'unpause':
				c.unpause()
			else:
				return response.bad_request('Unknown action.')

			return response.success({'result': {c.id: as_dict(c)}})
		except docker.errors.NotFound as e:
			return response.not_found('A container with that ID does not exist.')

class ContainerLogs(Resource):
	_endpoint = '/<container_id>/logs'

	@require_key
	@permission('containers.select')
	def get(self, user, container_id):
		try:
			return response.success({'result': client.containers.get(container_id).logs().split('\n')})
		except docker.errors.NotFound as e:
			return response.not_found('A container with that ID does not exist.')
