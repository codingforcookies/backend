import os


SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL', 'sqlite:///test.db')
secret_key = 'mJ9bQy2vAGXEJMLcMbalZy4SHhHRzsBZdnlOAvoZtfXKzAoGB3x6NPZGdU3KJP7X'

db = None

def init_db(app):
	global db

	app.config['SQLALCHEMY_DATABASE_URI'] = SQLALCHEMY_DATABASE_URI
	app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
	app.secret_key = secret_key

	print('Database: %s' % SQLALCHEMY_DATABASE_URI)

	from flask_sqlalchemy import SQLAlchemy
	db = SQLAlchemy(app)

	return db

def update(changes, obj, obj_name, data, name):
	if name not in data: return

	if getattr(obj, obj_name) != data[name]:
		changes.append('%s: %s >> %s' % (name.replace('_', ' ').title(), getattr(obj, obj_name), data[name]))
		setattr(obj, obj_name, data[name])
