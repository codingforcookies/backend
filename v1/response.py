# POST is most-often utilized to create new resources. In particular, it's used to
# create subordinate resources. That is, subordinate to some other (e.g. parent)
# resource. In other words, when creating a new resource, POST to the parent and
# the service takes care of associating the new resource with the parent, assigning
# an ID (new resource URI), etc. On successful creation, return HTTP status 201,
# returning a Location header with a link to the newly-created resource with the
# 201 HTTP status.

# GET method is used to read (or retrieve) a representation of a resource. In the
# "happy" (or non-error) path, GET returns a representation in XML or JSON and an
# HTTP response code of 200 (OK). In an error case, it most often returns a 404
# (NOT FOUND) or 400 (BAD REQUEST). According to the design of the HTTP
# specification, GET (along with HEAD) requests are used only to read data and not
# change it.

# PUT is most-often utilized for update capabilities, PUT-ing to a known resource
# URI with the request body containing the newly-updated representation of the
# original resource.However, PUT can also be used to create a resource in the case
# where the resource ID is chosen by the client instead of by the server. In other
# words, if the PUT is to a URI that contains the value of a non-existent resource ID

# DELETE is used to delete a resource identified by a URI.HTTP-spec-wise, DELETE
# operations are idempotent. If you DELETE a resource, it's removed On successful
# deletion, return HTTP status 200 (OK) along with a response body.

from flask import request
from flask_restful import reqparse


# Success. Content should be supplied.
def success(data):
	data['status'] = 200
	return data, 200

# Indicates that the resource was created successfully.
#
# Header: Location should be set to the URI for the new resource.
def created(data, location):
	data['status'] = 201
	return data, 201, {'Location': location}

# Success, but no content needs to be returned.
def no_content():
	return {'status': 204}, 204

# Nothing was changed.
def not_modified():
	return {'status': 304}, 304

# Something wrong with input.
def bad_request(error):
	return {'status': 400, 'message': error}, 400

# You need to be logged in to do that.
def unauthorized():
	return {'status': 401, 'message': 'You must provide an API token to do that.'}, 401

# You aren't allowed to access that.
def forbidden():
	return {'status': 403, 'message': 'You do not have permission to do that.'}, 403

# That resource doesn't exist.
def not_found(error):
	return {'status': 404, 'message': error}, 404

# Indicates that the method used is not allowed.
def invalid_method():
	return {'status': 405, 'message': 'Method not allowed.'}, 405

# Attempted to create a resource, but one of that type already exists.
def conflict(error):
	return {'status': 409, 'message': error}, 409

# Indicates that the user has submitted too many requests in a time period.
def rate_limited():
	return {'status': 429, 'message': 'Too many requests.'}, 429

# Internal server error.
def internal_error(error):
	return {'status': 500, 'message': error}, 500

# Indicates that the requested feature has not yet been implemented.
def not_implemented():
	return {'status': 501, 'message': 'Not implemented.'}, 501

# A decorator for api endpoints. Required endpoint arguments.
def rargs(*rargs):
	parser = reqparse.RequestParser()
	for arg in rargs:
		parser.add_argument(arg[1], type=arg[0])
	def wrap(fn):
		def inner(*args, **kwargs):
			p_args = parser.parse_args()
			found = 0
			for arg in rargs:
				if arg[1] in p_args:
					found += 1
			if found is not len(p_args):
				return Response.bad_request('Did not supply required arguments.')
			kwargs.update(p_args)
			return fn(*args, **kwargs)
		return inner
	return wrap

# A decorator for api endpoints. Possible endpoint arguments.
def pargs(*args):
	parser = reqparse.RequestParser()
	for arg in args:
		parser.add_argument(arg[1], type=arg[0])
	def wrap(fn):
		def inner(*args, **kwargs):
			p_args = parser.parse_args()
			kwargs.update(p_args)
			return fn(*args, **kwargs)
		return inner
	return wrap

def is_ajax():
	if request.headers.has_key('X-Requested-With'):
		if request.headers.get('X-Requested-With') == 'XMLHttpRequest':
			return True
	return False
