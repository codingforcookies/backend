from flask import request
from flask_restful import Resource
from sqlalchemy.exc import IntegrityError

from .. import require_key, permission, response
from ..db import db, update

from .table import Role, Permission, User, AccessKey


adjectives = []
nouns = []
with open('./adjectives.txt', 'r') as f:
	for line in f:
		adjectives.append(line.strip())
with open('./nouns.txt', 'r') as f:
	for line in f:
		nouns.append(line.strip())

class UserList(Resource):
	_endpoint = ''

	@require_key
	@permission('user.list')
	def get(self, user):
		return response.success({
					'result': [i.as_dict() for i in User.query.all()]
				})

	@require_key
	@permission('user.new')
	def post(self, user):
		data = request.get_json(silent=True)
		if data is None: return response.bad_request('JSON data must be supplied.')

		try:
			row = User(data['username'], random.choice(adjectives) + random.choice(adjectives) + random.choice(nouns))
			row.role = data['role']
			db.session.add(row)
			db.session.commit()

			return response.created({'result': row.as_dict()}, '/users/%i' % row.user_id)
		except IntegrityError as e:
			return response.conflict('User already exists.')

class UserKey(Resource):
	_endpoint = '/login'

	def post(self):
		data = request.get_json(silent=True)
		if data is None: return response.bad_request('JSON data must be supplied.')

		user = User.query.filter_by(username=data['username']).first();
		if user is None: return response.not_found('A user with that name does not exist.')

		if user.gen_hash(data['password']) != user.password: return response.bad_request('Invalid password.')

		key, access = AccessKey.create(user.user_id)
		db.session.add(access)
		db.session.commit()

		return response.success({'result': key})

class UserSelect(Resource):
	_endpoint = '/<int:user_id>'

	@require_key
	@permission('user.select')
	def get(self, user, user_id):
		row = User.query.filter_by(user_id=user_id).first()
		if row is None: return response.not_found('A user with that ID does not exist.')
		return response.success({'result': row.as_dict()})

	@require_key
	@permission('user.update')
	def put(self, user, user_id):
		data = request.get_json(silent=True)
		if data is None: return response.bad_request('JSON data must be supplied.')

		row = User.query.filter_by(user_id=user_id).limit(1).first()
		if row is None: return response.not_found('A user with that ID does not exist.')

		return response.success({'result': row.as_dict()})

	@require_key
	@permission('user.delete')
	def delete(self, user, user_id):
		row = User.query.filter_by(user_id=user_id).limit(1).first()
		if row is None: return response.not_found('A user with that ID does not exist.')

		return response.no_content()



class RoleList(Resource):
	_endpoint = '/roles'

	@require_key
	@permission('role.list')
	def get(self):
		page = request.args.get('page', default=0, type=int)
		count = request.args.get('count', default=10, type=int)

		result = [i.as_dict() for i in Role.query.offset(page * count).limit(count).all()]

		return response.success({
					'page': page,
					'count': count,
					'result': result
				})

	@require_key
	@permission('role.new')
	def post(self, user):
		data = request.get_json(silent=True)
		if data is None: return response.bad_request('JSON data must be supplied.')

		try:
			row = Role(data['name'])
			db.session.add(row)
			db.session.commit()

			return response.created({'result': row.as_dict()}, '/users/role/%i' % row.role_id)
		except IntegrityError as e:
			return response.conflict('User already exists.')

class RoleSelect(Resource):
	_endpoint = '/roles/<int:role_id>'

	@require_key
	@permission('role.select')
	def get(self, user, role_id):
		row = Role.query.filter_by(role_id=role_id).first()
		if row is None: return response.not_found('A role with that ID does not exist.')
		return response.success({'result': row.as_dict()})

	@require_key
	@permission('role.update')
	def put(self, user, role_id):
		data = request.get_json(silent=True)
		if data is None: return response.bad_request('JSON data must be supplied.')

		row = Role.query.filter_by(role_id=role_id).limit(1).first()
		if row is None: return response.not_found('A role with that ID does not exist.')

		old_name = row.role_name

		changes = []
		update(changes, row, 'role', data, 'role')
		if 'permissions' in data:
			existing = []
			for perm in row.permissions:
				if perm.node not in data['permissions']:
					db.session.delete(perm)
				else:
					existing.append(perm.node)
			for node in data['permissions']:
				if node not in existing:
					db.session.add(Permission(row.role_id, node))

		return response.success({'result': row.as_dict()})

	@require_key
	@permission('role.delete')
	def delete(self, user, role_id):
		row = Role.query.filter_by(role_id=role_id).limit(1).first()
		if row is None: return response.not_found('A role with that ID does not exist.')

		return response.no_content()
