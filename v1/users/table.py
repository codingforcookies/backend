import hashlib
import uuid
import binascii
from datetime import datetime, timedelta
import random
import string

import sqlalchemy as sq
import sqlalchemy.orm as orm

from ..db import db


class Role(db.Model):
	__tablename__ = 'lovpanel_roles'

	role_id = sq.Column(sq.Integer, primary_key=True)
	role_name = sq.Column(sq.String(16))

	permissions = orm.relationship('Permission', backref='role')

	def __init__(self, role_name):
		self.role_name = role_name

	def as_dict(self):
		return {
				'id': self.role_id,
				'name': self.role_name,
				'permissions': [p.node for p in self.permissions]
			}

	def __repr__(self):
		return '<Role %r>' % self.role_name

class Permission(db.Model):
	__tablename__ = 'lovpanel_role_permissions'

	role_id = sq.Column(sq.Integer, sq.ForeignKey(Role.role_id, ondelete='CASCADE'), primary_key=True)
	node = sq.Column(sq.String(32), primary_key=True)

	def __init__(self, role_id, node):
		self.role_id = role_id
		self.node = node

	def as_dict(self):
		return {
				'id': self.role_id,
				'node': self.node
			}

	def __repr__(self):
		return '<Permission %r>' % self.node

class User(db.Model):
	__tablename__ = 'lovpanel_users'

	user_id = sq.Column(sq.Integer, primary_key=True)
	role_id = sq.Column(sq.Integer, sq.ForeignKey(Role.role_id))
	username = sq.Column(sq.String(32), unique=True)
	password_salt = sq.Column(sq.String(32))
	password = sq.Column(sq.String(64))

	role = orm.relationship(Role, backref='users')

	def __init__(self, role_id, username, password):
		self.role_id = role_id
		self.username = username
		self.set_password(password)

	def set_password(self, password):
		self.password_salt = uuid.uuid4().hex
		self.password = self.gen_hash(password)

	def gen_hash(self, password=None):
		if password is None:
			password = self.password
		the_hash = hashlib.pbkdf2_hmac('sha256', password.encode('utf-8'), self.password_salt.encode('utf-8'), 100000)
		return binascii.hexlify(the_hash).decode('utf-8')

	def as_dict(self):
		return {
				'user_id': self.user_id,
				'role': self.role.as_dict(),
				'username': self.username
			}

	def __repr__(self):
		return '<User %r>' % self.username

class AccessKey(db.Model):
	__tablename__ = 'lovpanel_access_key'

	user_id = sq.Column(sq.Integer, sq.ForeignKey(User.user_id, ondelete='CASCADE'), primary_key=True)
	key_hash = sq.Column(sq.String(64), primary_key=True)
	created_at = sq.Column(sq.DateTime)
	updated_at = sq.Column(sq.DateTime)
	expires_at = sq.Column(sq.DateTime)

	user = orm.relationship(User, backref='access_keys')

	def create(user_id):
		rnd = random.SystemRandom()
		key = ''.join(rnd.choice(string.ascii_letters + string.digits) for i in range(32))
		return key, AccessKey(user_id, key)

	def gen_hash(key):
		the_hash = hashlib.pbkdf2_hmac('sha256', key.encode('UTF-8'), b'th6j732hk7jryHSTrg4gH7jhsdtw4h5A', 100000)
		return binascii.hexlify(the_hash).decode('utf-8')

	def __init__(self, user_id, key):
		self.user_id = user_id
		self.created_at = datetime.now()
		self.updated_at = datetime.now()
		self.expires_at = datetime.now() + timedelta(days=1)
		self.key_salt = uuid.uuid4().hex
		self.key_hash = AccessKey.gen_hash(key)

	def validate(self):
		if self.expires_at < datetime.now():
			db.session.delete(self)
			db.session.commit()
			return False

		self.updated_at = datetime.now()
		db.session.add(self)
		db.session.commit()
		return True

	def as_dict(self):
		return {
				'user': self.user.as_dict(),
				'time': {
					'created': self.created_at,
					'expired': self.updated_at
				}
			}

	def __repr__(self):
		return '<AccessKey %r>' % self.user_id
