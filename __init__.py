import os

from flask import Flask
from flask_cors import CORS
from flask_restful import Resource

from v1 import api_init


app = Flask(__name__)
cors = CORS(app, resources={r'/v1/*': {'origins': '*'}})

api = api_init(app, 'v1')

from v1.db import db
from v1.users.table import Role, Permission, User, AccessKey
if len(User.query.all()) == 0:
	role = Role('Administrator')
	db.session.add(role)
	db.session.flush()

	db.session.add(Permission(role.role_id, '*'))

	user = User(role.role_id, 'admin', 'test')
	db.session.add(user)
	db.session.flush()

	key, access_key = AccessKey.create(user.user_id)
	db.session.add(access_key)

	db.session.commit()

	print('Key generated: %s' % key)

app.run(host='0.0.0.0', port=int(os.environ.get('PORT', 13370)), threaded=True)
